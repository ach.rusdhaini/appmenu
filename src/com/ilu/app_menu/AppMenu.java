package com.ilu.app_menu;

import com.ilu.app_menu.menus.MainMenu;
import com.ilu.app_menu.models.Menu;

public class AppMenu {
    public static void main(String[] args) {

        new MainMenu();

        Menu.exitMenu();
    }
}
