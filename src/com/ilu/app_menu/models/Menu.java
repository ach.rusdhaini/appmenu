package com.ilu.app_menu.models;

import java.util.Arrays;
import java.util.Map;

public class Menu {
    private String header;
    private Map<String, String> menuMap;

    public Menu(String header, Map<String, String> menuMap) {
        this.header = header;
        this.menuMap = menuMap;
    }

    public String getHeader() {
        return header;
    }

    public Map<String, String> getMenuMap() {
        return menuMap;
    }

    public void showMenus(String note, String action, String[] data) {

        String fullHeader = "\n=====+=====  ANDA BERADA DI ".concat(header.toUpperCase()).concat(" =====+=====");
        int fullHeaderLength = fullHeader.length();

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println(fullHeader);

        for (Map.Entry<String, String> entry : menuMap.entrySet()) {
            String menu = entry.getKey() + ". " + entry.getValue();
            System.out
                    .println("== ".concat(menu).concat(" ".repeat((fullHeaderLength % 2 == 0 ? fullHeaderLength : fullHeaderLength - 1) - menu.length() - 6)).concat(" =="));
        }

        System.out.println("=====+".concat("=".repeat(fullHeaderLength - 12)).concat("+====="));

        if (note != null && note.length() > 0) {
            System.out.print("");
            System.out.println(note);
            System.out.println();
        }

        if (data != null) {
            System.out.println("Book list : " + Arrays.toString(data));
        }

        if (action != null && action.length() > 0) {
            System.out.print(action);
        } else {
            System.out.print("Navigate to menu : ");
        }

    }

    public boolean isValidMenu(String option) {
        return menuMap.containsKey(String.valueOf(option));
    }

    public static void exitMenu() {
        System.out.println("Exit menu");
        System.exit(0);
    }
}
