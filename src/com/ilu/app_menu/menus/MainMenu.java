package com.ilu.app_menu.menus;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import com.ilu.app_menu.models.Menu;

public class MainMenu {
    public MainMenu() {

        Scanner scanner = new Scanner(System.in);

        Map<String, String> menuMap = new LinkedHashMap<String, String>() {{
            put("1", "Simple Calculator");
            put("2", "Book CRUD");
            put("3", "Menu C");
            put("00", "Keluar");
        }};
        Menu menu = new Menu("Main Menu", menuMap);

        String option;
        do {
            menu.showMenus(null, null, null);
            option = scanner.nextLine();

            if (menu.isValidMenu(option)) {
                if (option.equals("1")) {
                    new Calculator();
                }

                if (option.equals("2")) {
                    BookCRUD bookCRUD = new BookCRUD();
                    bookCRUD.run();
                }

                if (option.equals("00")) {
                    break;
                }
            } else {
                System.out.println("404 menu not found");
            }
        } while (!option.equals("00"));

        scanner.close();
    }
}
