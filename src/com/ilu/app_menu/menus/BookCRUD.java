package com.ilu.app_menu.menus;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import com.ilu.app_menu.models.Menu;

public class BookCRUD {

    // private ArrayList<String> bookArray = new ArrayList<>();
    private String[] bookArray = new String[0];

    public void run() {
        Scanner scanner = new Scanner(System.in);

        Map<String, String> menuMap = new LinkedHashMap<String, String>() {
            {
                put("1", "Add");
                put("2", "Delete");
                put("3", "Show");
                put("4", "Search");
                put("5", "Update");
                put("0", "Back");
                put("00", "Keluar");
            }
        };
        Menu menu = new Menu("Book CRUD", menuMap);

        String action;

        do {
            menu.showMenus(null, "fill the navigation options or book name : ", bookArray);

            action = scanner.nextLine();

            if (menu.isValidMenu(action)) {
                if (action.equals("1")) {
                    addBook(scanner);
                } else if (action.equals("2")) {
                    deleteBook(scanner);
                } else if (action.equals("3")) {
                    showBook(true);
                } else if (action.equals("4")) {
                    searchBook(scanner);
                } else if (action.equals("5")) {
                    updateBook(scanner);
                } else if (action.equals("0")) {
                    new MainMenu();
                } else {
                    System.out.println("Something went wrong");
                }
            } else {
                System.out.println("404 menu not found");
            }

        } while (!action.equals("00"));

        scanner.close();
    }

    private void addBook(Scanner scanner) {
        Map<String, String> menuMap = new LinkedHashMap<String, String>() {
            {
                put("1", "Back");
                put("00", "Keluar");
            }
        };
        Menu menu = new Menu("Book CRUD", menuMap);

        String action;
        int currentLength = bookArray.length;

        do {
            menu.showMenus(null, "Input book name or select menu option : ", bookArray);

            action = scanner.nextLine();

            if (action.equals("1")) {
                run();
            } else if (action.equals("00")) {
                Menu.exitMenu();
            } else {
                String bookName = action;

                bookArray = addToArray(bookArray, bookName);
                int newLength = bookArray.length;
                
                if (currentLength < newLength) {
                    System.out.println("Book added successfully");
                } else {
                    System.out.println("Something went wrong");
                }
            }
        } while (!action.equals("00"));

        scanner.close();
    }

    private void searchBook(Scanner scanner) {
        Map<String, String> menuMap = new LinkedHashMap<String, String>() {
            {
                put("1", "Back");
                put("00", "Keluar");
            }
        };
        Menu menu = new Menu("Book CRUD", menuMap);

        String action;

        do {
            menu.showMenus(null, "Search book name or select menu option : ", bookArray);

            action = scanner.nextLine();

            if (action.equals("1")) {
                run();
            } else if (action.equals("00")) {
                Menu.exitMenu();
            } else {
                String bookName = action;

                int bookIndex = findIndex(bookArray, bookName);
                if (bookIndex != -1) {
                    System.out.println("Book with name " + bookName + " found in index " + bookIndex);
                } else {
                    System.out.println("Book with name " + bookName + " not found");
                }
            }
        } while (!action.equals("00"));

        scanner.close();
    }

    private void deleteBook(Scanner scanner) {
        Map<String, String> menuMap = new LinkedHashMap<String, String>() {
            {
                put("1", "Back");
                put("00", "Keluar");
            }
        };
        Menu menu = new Menu("Book CRUD", menuMap);

        String action;

        do {

            showBook(false);
            menu.showMenus(null, "Search for the name of the book to delete or select a menu option : ", bookArray);

            action = scanner.nextLine();

            if (action.equals("1")) {
                run();
            } else if (action.equals("00")) {
                Menu.exitMenu();
            } else {
                String bookName = action;

                int bookIndex = findIndex(bookArray, bookName);
                if (bookIndex != -1) {
                    bookArray = removeFromArray(bookArray, bookIndex);
                    System.out.println("Book with name " + bookName + " deleted successfully");
                } else {
                    System.out.println("Book with name " + bookName + " not found");
                }
            }
        } while (!action.equals("00"));

        scanner.close();
    }
    
    private void updateBook(Scanner scanner) {
        Map<String, String> menuMap = new LinkedHashMap<String, String>() {
            {
                put("1", "Back");
                put("00", "Keluar");
            }
        };
        Menu menu = new Menu("Book CRUD", menuMap);

        String action;

        do {

            showBook(false);
            menu.showMenus(null, "Search for the name of the book to update or select a menu option : ", bookArray);

            action = scanner.nextLine();

            if (action.equals("1")) {
                run();
            } else if (action.equals("00")) {
                Menu.exitMenu();
            } else {
                String bookName = action;

                int bookIndex = findIndex(bookArray, bookName);
                if (bookIndex != -1) {
                    System.out.print("Enter new name for the book : ");
                    String newBookName = scanner.nextLine();
                    
                    bookArray[bookIndex] = newBookName;
                    System.out.println("Book with name " + bookName + " changed to " + newBookName);
                    
                } else {
                    System.out.println("Book with name " + bookName + " not found");
                }
            }
        } while (!action.equals("00"));

        scanner.close();
    }

    private static String[] removeFromArray(String[] arr, int index) 
    { 
        if (arr == null || index < 0
            || index >= arr.length) { 
  
            return arr; 
        } 
  
        String[] anotherArray = new String[arr.length - 1]; 
  
        for (int i = 0, k = 0; i < arr.length; i++) { 
  
            if (i == index) { 
                continue; 
            } 
  
            anotherArray[k++] = arr[i]; 
        } 
  
        // return the resultant array 
        return anotherArray; 
    } 

    public static String[] addToArray(String[] originalArray, String newElement) {
        String[] newArray = Arrays.copyOf(originalArray, originalArray.length + 1);

        newArray[newArray.length - 1] = newElement;
        return newArray;
    }

    private void showBook(boolean isRun) {
        System.out.println(Arrays.toString(bookArray));

        if (isRun) {
            run();
        }
    }

    private static int findIndex(String[] array, String element) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element)) {
                return i;
            }
        }

        return -1;
    }
}
